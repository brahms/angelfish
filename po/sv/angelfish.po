# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-angelfish package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-angelfish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-14 00:47+0000\n"
"PO-Revision-Date: 2022-04-28 17:16+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stefan Asserhäll"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "stefan.asserhall@bredband.net"

#: angelfish-webapp/contents/ui/WebAppView.qml:31
#: lib/contents/ui/WebView.qml:68
#, kde-format
msgid "Copy"
msgstr "Kopiera"

#: angelfish-webapp/contents/ui/WebAppView.qml:36
#: lib/contents/ui/WebView.qml:73
#, kde-format
msgid "Cut"
msgstr "Klipp ut"

#: angelfish-webapp/contents/ui/WebAppView.qml:41
#: lib/contents/ui/WebView.qml:78
#, kde-format
msgid "Paste"
msgstr "Klistra in"

#: angelfish-webapp/contents/ui/WebAppView.qml:46
#: lib/contents/ui/WebView.qml:83
#, kde-format
msgid "Search online for '%1'"
msgstr "Sök på nätet efter '%1'"

#: angelfish-webapp/contents/ui/WebAppView.qml:46
#: lib/contents/ui/WebView.qml:83
#, kde-format
msgid "Search online"
msgstr "Sök på nätet"

#: angelfish-webapp/contents/ui/WebAppView.qml:51
#: lib/contents/ui/WebView.qml:88
#, kde-format
msgid "Copy Url"
msgstr "Kopiera webbadress"

#: angelfish-webapp/contents/ui/WebAppView.qml:55
#: lib/contents/ui/DownloadQuestion.qml:20 lib/contents/ui/WebView.qml:96
#: src/contents/ui/AdblockFilterDownloadQuestion.qml:25
#, kde-format
msgid "Download"
msgstr "Ladda ner"

#: angelfish-webapp/main.cpp:63
#, kde-format
msgid "desktop file to open"
msgstr "skrivbordsfil att öppna"

#: angelfish-webapp/main.cpp:90
#, kde-format
msgid "Angelfish Web App runtime"
msgstr "Angelfish webbkörprogram"

#: angelfish-webapp/main.cpp:92
#, kde-format
msgid "Copyright 2020 Angelfish developers"
msgstr "Copyright 2020, Angelfish-utvecklarna"

#: angelfish-webapp/main.cpp:94
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: lib/angelfishwebprofile.cpp:58
#, kde-format
msgid "Download finished"
msgstr "Nerladdning klar"

#: lib/angelfishwebprofile.cpp:62
#, kde-format
msgid "Download failed"
msgstr "Nerladdning misslyckades"

#: lib/contents/ui/AuthSheet.qml:21
#, kde-format
msgid "Authentication required"
msgstr "Behörighetskontroll krävs"

#: lib/contents/ui/AuthSheet.qml:30
#, kde-format
msgid "Username"
msgstr "Användarnamn"

#: lib/contents/ui/AuthSheet.qml:37
#, kde-format
msgid "Password"
msgstr "Lösenord"

#: lib/contents/ui/AuthSheet.qml:46 lib/contents/ui/PermissionQuestion.qml:43
#, kde-format
msgid "Accept"
msgstr "Acceptera"

#: lib/contents/ui/AuthSheet.qml:55 lib/contents/ui/DownloadQuestion.qml:28
#: lib/contents/ui/JavaScriptDialogSheet.qml:43
#: lib/contents/ui/JavaScriptDialogSheet.qml:66
#: src/contents/ui/Downloads.qml:41
#, kde-format
msgid "Cancel"
msgstr "Avbryt"

#: lib/contents/ui/DownloadQuestion.qml:12
#, kde-format
msgid "Do you want to download this file?"
msgstr "Vill du ladda ner den här filen?"

#: lib/contents/ui/ErrorHandler.qml:47
#, kde-format
msgid "Error loading the page"
msgstr "Fel vid inläsning av sidan"

#: lib/contents/ui/ErrorHandler.qml:58
#, kde-format
msgid "Retry"
msgstr "Försök igen"

#: lib/contents/ui/ErrorHandler.qml:71
#, kde-format
msgid ""
"Do you wish to continue?\n"
"\n"
" If you wish so, you may continue with an unverified certificate.\n"
" Accepting an unverified certificate means\n"
" you may not be connected with the host you tried to connect to.\n"
" Do you wish to override the security check and continue?"
msgstr ""
"Vill du fortsätta?\n"
"\n"
"Om du önskar kan du fortsätta med ett overifierat certifikat.\n"
"Att acceptera en overifierat certifikat betyder att du kanske\n"
"inte är anslutna med värddatorn du försökte ansluta till.\n"
"Vill du överskrida säkerhetskontrollen och fortsätta?"

#: lib/contents/ui/ErrorHandler.qml:79
#, kde-format
msgid "Yes"
msgstr "Ja"

#: lib/contents/ui/ErrorHandler.qml:88
#, kde-format
msgid "No"
msgstr "Nej"

#: lib/contents/ui/JavaScriptDialogSheet.qml:21
#, kde-format
msgid "Close"
msgstr "Stäng"

#: lib/contents/ui/JavaScriptDialogSheet.qml:35
#, kde-format
msgid "Confirm"
msgstr "Bekräfta"

#: lib/contents/ui/JavaScriptDialogSheet.qml:75
#, kde-format
msgid "Submit"
msgstr "Skicka"

#: lib/contents/ui/JavaScriptDialogSheet.qml:89
#, kde-format
msgid "Leave page"
msgstr "Lämna sida"

#: lib/contents/ui/JavaScriptDialogSheet.qml:129
#, kde-format
msgid ""
"The website asks for confirmation that you want to leave. Unsaved "
"information might not be saved."
msgstr ""
"Webbsidan frågar efter bekräftelse att du vill lämna den. Osparad "
"information kanske inte sparas."

#: lib/contents/ui/PermissionQuestion.qml:16
#, kde-format
msgid "Do you want to allow the website to access the geo location?"
msgstr "Vill du att webbplatsen ska kunna komma åt geografisk plats?"

#: lib/contents/ui/PermissionQuestion.qml:19
#, kde-format
msgid "Do you want to allow the website to access the microphone?"
msgstr "Vill du att webbplatsen ska kunna komma åt mikrofonen?"

#: lib/contents/ui/PermissionQuestion.qml:22
#, kde-format
msgid "Do you want to allow the website to access the camera?"
msgstr "Vill du att webbplatsen ska kunna komma åt kameran?"

#: lib/contents/ui/PermissionQuestion.qml:25
#, kde-format
msgid ""
"Do you want to allow the website to access the camera and the microphone?"
msgstr "Vill du att webbplatsen ska kunna komma åt kameran och mikrofonen?"

#: lib/contents/ui/PermissionQuestion.qml:28
#, kde-format
msgid "Do you want to allow the website to share your screen?"
msgstr "Vill du att webbplatsen ska kunna dela bildskärmen?"

#: lib/contents/ui/PermissionQuestion.qml:31
#, kde-format
msgid "Do you want to allow the website to share the sound output?"
msgstr "Vill du att webbplatsen ska kunna dela ljudutmatning?"

#: lib/contents/ui/PermissionQuestion.qml:34
#, kde-format
msgid "Do you want to allow the website to send you notifications?"
msgstr "Vill du att webbplatsen ska kunna skicka dig underrättelser?"

#: lib/contents/ui/PermissionQuestion.qml:53
#, kde-format
msgid "Decline"
msgstr "Neka"

#: lib/contents/ui/WebView.qml:92
#, kde-format
msgid "View source"
msgstr "Visa källkoden"

#: lib/contents/ui/WebView.qml:101
#, kde-format
msgid "Open in new Tab"
msgstr "Öppna under ny flik"

#: lib/contents/ui/WebView.qml:269
#, kde-format
msgid "Website was opened in a new tab"
msgstr "Webbplatsen öppnades under en ny flik"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:14
#, kde-format
msgid ""
"The ad blocker is missing its filter lists, do you want to download them now?"
msgstr "Reklamblockeringen saknar sina filterlistor. Vill du ladda ner dem nu?"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:30
#, kde-format
msgid "Downloading..."
msgstr "Laddar ner..."

#: src/contents/ui/Bookmarks.qml:12 src/contents/ui/desktop/desktop.qml:250
#: src/contents/ui/mobile.qml:77
#, kde-format
msgid "Bookmarks"
msgstr "Bokmärken"

#: src/contents/ui/desktop/BookmarksPage.qml:28
#, kde-format
msgid "Search bookmarks…"
msgstr "Sök bokmärken…"

#: src/contents/ui/desktop/BookmarksPage.qml:70
#, kde-format
msgid "No bookmarks yet"
msgstr "Inga bokmärken ännu"

#: src/contents/ui/desktop/desktop.qml:232 src/contents/ui/Tabs.qml:67
#, kde-format
msgid "New Tab"
msgstr "Ny flik"

#: src/contents/ui/desktop/desktop.qml:241 src/contents/ui/History.qml:12
#: src/contents/ui/mobile.qml:85
#, kde-format
msgid "History"
msgstr "Historik"

#: src/contents/ui/desktop/desktop.qml:259 src/contents/ui/Downloads.qml:15
#: src/contents/ui/mobile.qml:89
#, kde-format
msgid "Downloads"
msgstr "Nerladdningar"

#: src/contents/ui/desktop/desktop.qml:270
#, kde-format
msgid "Full Screen"
msgstr "Fullskärm"

#: src/contents/ui/desktop/desktop.qml:284 src/contents/ui/mobile.qml:381
#, kde-format
msgid "Show developer tools"
msgstr "Visa utvecklingsverktyg"

#: src/contents/ui/desktop/desktop.qml:296 src/contents/ui/mobile.qml:281
#, kde-format
msgid "Find in page"
msgstr "Sök på sidan"

#: src/contents/ui/desktop/desktop.qml:305
#, kde-format
msgctxt "@action:inmenu"
msgid "Reader Mode"
msgstr ""

#: src/contents/ui/desktop/desktop.qml:311
#, kde-format
msgid "Add to application launcher"
msgstr "Lägg till i startprogram"

#: src/contents/ui/desktop/desktop.qml:330 src/contents/ui/mobile.qml:97
#, kde-format
msgid "Settings"
msgstr "Inställningar"

#: src/contents/ui/desktop/desktop.qml:337 src/contents/ui/mobile.qml:104
#, kde-format
msgid "Configure Angelfish"
msgstr "Anpassa Angelfish"

#: src/contents/ui/desktop/HistoryPage.qml:28
#, kde-format
msgid "Search history…"
msgstr "Sökhistorik…"

#: src/contents/ui/desktop/HistoryPage.qml:44
#, kde-format
msgid "Clear all history"
msgstr "Rensa all historik"

#: src/contents/ui/desktop/HistoryPage.qml:80
#, kde-format
msgid "Not history yet"
msgstr "Ingen historik ännu"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:12
#: src/contents/ui/SettingsPage.qml:32
#, kde-format
msgid "Toolbars"
msgstr "Verktygsrader"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:18
#, kde-format
msgid "Show home button:"
msgstr "Visa hemknapp:"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:19
#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:77
#, kde-format
msgid "Enabled"
msgstr "Aktiverad"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:19
#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:77
#, kde-format
msgid "Disabled"
msgstr "Inaktiverad"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:26
#, kde-format
msgid "Homepage:"
msgstr "Hemsida:"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:51
#, kde-format
msgid "New tabs:"
msgstr "Nya flikar:"

#: src/contents/ui/desktop/settings/HomeSettingsPage.qml:76
#, kde-format
msgid "Always show the tab bar:"
msgstr "Visa alltid flikraden:"

#: src/contents/ui/desktop/Tabs.qml:189 src/contents/ui/Tabs.qml:233
#, kde-format
msgid "Reader Mode: %1"
msgstr ""

#: src/contents/ui/Downloads.qml:26
#, kde-format
msgid "No running downloads"
msgstr "Inga pågående nerladdningar"

#: src/contents/ui/Downloads.qml:47
#, kde-format
msgid "Pause"
msgstr "Paus"

#: src/contents/ui/Downloads.qml:53
#, kde-format
msgid "Continue"
msgstr "Fortsätt"

#: src/contents/ui/Downloads.qml:91
#, kde-format
msgctxt "download state"
msgid "Starting…"
msgstr "Startar…"

#: src/contents/ui/Downloads.qml:93
#, kde-format
msgid "Completed"
msgstr "Klar"

#: src/contents/ui/Downloads.qml:95
#, kde-format
msgid "Cancelled"
msgstr "Avbruten"

#: src/contents/ui/Downloads.qml:97
#, kde-format
msgctxt "download state"
msgid "Interrupted"
msgstr "Avbruten"

#: src/contents/ui/Downloads.qml:99
#, kde-format
msgctxt "download state"
msgid "In progress"
msgstr "Pågår"

#: src/contents/ui/FindInPageBar.qml:46
#, kde-format
msgid "Search..."
msgstr "Sök..."

#: src/contents/ui/InputSheet.qml:47
#, kde-format
msgid "OK"
msgstr "Ok"

#: src/contents/ui/mobile.qml:18
#, kde-format
msgid "Angelfish Web Browser"
msgstr "Angelfish webbläsare"

#: src/contents/ui/mobile.qml:62 src/contents/ui/Tabs.qml:63
#, kde-format
msgid "Tabs"
msgstr "Flikar"

#: src/contents/ui/mobile.qml:69
#, kde-format
msgid "Leave private mode"
msgstr "Lämna privat läge"

#: src/contents/ui/mobile.qml:69
#, kde-format
msgid "Private mode"
msgstr "Privat läge"

#: src/contents/ui/mobile.qml:285 src/contents/ui/ShareSheet.qml:18
#, kde-format
msgid "Share page"
msgstr "Dela sida"

#: src/contents/ui/mobile.qml:296
#, kde-format
msgid "Add to homescreen"
msgstr "Lägg till i på hemsida"

#: src/contents/ui/mobile.qml:306
#, kde-format
msgid "Open in app"
msgstr "Öppna i program"

#: src/contents/ui/mobile.qml:314
#, kde-format
msgid "Go previous"
msgstr "Gå till föregående"

#: src/contents/ui/mobile.qml:322
#: src/contents/ui/SettingsNavigationBarPage.qml:109
#, kde-format
msgid "Go forward"
msgstr "Gå framåt"

#: src/contents/ui/mobile.qml:329
#, kde-format
msgid "Stop loading"
msgstr "Stoppa inläsning"

#: src/contents/ui/mobile.qml:329
#, kde-format
msgid "Refresh"
msgstr "Uppdatera"

#: src/contents/ui/mobile.qml:339
#, kde-format
msgid "Bookmarked"
msgstr "Har bokmärke"

#: src/contents/ui/mobile.qml:339
#, kde-format
msgid "Bookmark"
msgstr "Bokmärke"

#: src/contents/ui/mobile.qml:355
#, kde-format
msgid "Show desktop site"
msgstr "Visa skrivbordsplats"

#: src/contents/ui/mobile.qml:364
#, kde-format
msgid "Reader Mode"
msgstr ""

#: src/contents/ui/mobile.qml:372
#, kde-format
msgid "Hide navigation bar"
msgstr "Dölj navigeringsrad"

#: src/contents/ui/mobile.qml:372
#, kde-format
msgid "Show navigation bar"
msgstr "Visa navigeringsrad"

#: src/contents/ui/NewTabQuestion.qml:11
#, kde-format
msgid ""
"Site wants to open a new tab: \n"
"%1"
msgstr ""
"Platsen vill öppna en ny flik: \n"
"%1"

#: src/contents/ui/NewTabQuestion.qml:19
#, kde-format
msgid "Open"
msgstr "Öppna"

#: src/contents/ui/SettingsAdblock.qml:17
#, kde-format
msgid "Adblock settings"
msgstr "Inställningar av reklamblockering"

#: src/contents/ui/SettingsAdblock.qml:27
#, kde-format
msgid "Update lists"
msgstr "Uppdatera listor"

#: src/contents/ui/SettingsAdblock.qml:48
#, kde-format
msgid "The adblock functionality isn't included in this build."
msgstr "Funktionen för reklamblockering ingår inte i programmet."

#: src/contents/ui/SettingsAdblock.qml:54
#, kde-format
msgid "Add filterlist"
msgstr "Lägg till filterlista"

#: src/contents/ui/SettingsAdblock.qml:59
#, kde-format
msgid "Name"
msgstr "Namn"

#: src/contents/ui/SettingsAdblock.qml:68
#, kde-format
msgid "Url"
msgstr "Webbadress"

#: src/contents/ui/SettingsAdblock.qml:78
#, kde-format
msgid "Add"
msgstr "Lägg till"

#: src/contents/ui/SettingsAdblock.qml:121
#, kde-format
msgid "Remove this filter list"
msgstr "Ta bort filterlistan"

#: src/contents/ui/SettingsGeneral.qml:20 src/contents/ui/SettingsPage.qml:12
#, kde-format
msgid "General"
msgstr "Allmänt"

#: src/contents/ui/SettingsGeneral.qml:28
#, kde-format
msgid "Enable JavaScript"
msgstr "Aktivera Javascript"

#: src/contents/ui/SettingsGeneral.qml:42
#, kde-format
msgid "Load images"
msgstr "Läs in bilder"

#: src/contents/ui/SettingsGeneral.qml:56
#, kde-format
msgid "Enable adblock"
msgstr "Aktivera reklamblockering"

#: src/contents/ui/SettingsGeneral.qml:73
#, kde-format
msgid ""
"When you open a link, image or media in a new tab, switch to it immediately"
msgstr ""
"När en länk, bild eller media öppnas i en ny flik, byt omedelbart till den"

#: src/contents/ui/SettingsNavigationBarPage.qml:14
#, kde-format
msgid "Navigation bar"
msgstr "Navigeringsrad"

#: src/contents/ui/SettingsNavigationBarPage.qml:35
#, kde-format
msgid ""
"Choose the buttons enabled in navigation bar. Some of the buttons can be "
"hidden only in portrait orientation of the browser and are always shown if  "
"the browser is wider than its height.\n"
"\n"
" Note that if you disable the menu buttons, you will be able to access the "
"menus either by swiping from the left or right side or to a side along the "
"bottom of the window."
msgstr ""
"Välj knapparna aktiverade på navigeringsraden. Några av knapparna kan bara "
"döljas i webbläsarens stående orientering, och visas alltid om webbläsaren "
"är bredare än sin höjd.\n"
"\n"
"Observera att om menyknapparna inaktiveras, går det att komma åt menyerna "
"genom att antingen svepa från vänster till höger sida eller mot en sida "
"längs fönstrets nederkant."

#: src/contents/ui/SettingsNavigationBarPage.qml:53
#, kde-format
msgid "Main menu in portrait"
msgstr "Stående huvudmeny"

#: src/contents/ui/SettingsNavigationBarPage.qml:67
#, kde-format
msgid "Tabs in portrait"
msgstr "Stående flikar"

#: src/contents/ui/SettingsNavigationBarPage.qml:81
#, kde-format
msgid "Context menu in portrait"
msgstr "Stående sammanhangsberoende meny"

#: src/contents/ui/SettingsNavigationBarPage.qml:95
#, kde-format
msgid "Go back"
msgstr "Gå tillbaka"

#: src/contents/ui/SettingsNavigationBarPage.qml:123
#, kde-format
msgid "Reload/Stop"
msgstr "Uppdatera/Stoppa"

#: src/contents/ui/SettingsPage.qml:17
#, kde-format
msgid "Ad Block"
msgstr "Reklamblockering"

#: src/contents/ui/SettingsPage.qml:22 src/contents/ui/SettingsWebApps.qml:13
#, kde-format
msgid "Web Apps"
msgstr "Webbprogram"

#: src/contents/ui/SettingsPage.qml:27
#: src/contents/ui/SettingsSearchEnginePage.qml:13
#: src/contents/ui/SettingsSearchEnginePage.qml:33
#, kde-format
msgid "Search Engine"
msgstr "Söktjänst"

#: src/contents/ui/SettingsSearchEnginePage.qml:28
#, kde-format
msgid "Custom"
msgstr "Egen"

#: src/contents/ui/SettingsSearchEnginePage.qml:34
#, kde-format
msgid "Base URL of your preferred search engine"
msgstr "Baswebbadress för söktjänsten som föredras"

#: src/contents/ui/SettingsWebApps.qml:44
#, kde-format
msgid "Remove app"
msgstr "Ta bort program"

#: src/contents/ui/Tabs.qml:63
#, kde-format
msgid "Private Tabs"
msgstr "Privata flikar"

#: src/main.cpp:71
#, kde-format
msgid "URL to open"
msgstr "Webbadress att öppna"

#~ msgid "General:"
#~ msgstr "Allmänt:"

#~ msgid "Enable Adblock"
#~ msgstr "Aktivera reklamblockering"

#~ msgid "Home"
#~ msgstr "Hem"

#~ msgid "Adblock filter lists"
#~ msgstr "Filterlista för reklamblockering"

#~ msgid "New"
#~ msgstr "Ny"

#~ msgid "Choose the buttons enabled in navigation bar. "
#~ msgstr "Välj knapparna som är aktiverade på navigeringsraden."

#~ msgid "Website that should be loaded on startup"
#~ msgstr "Webbsida som ska läsas in vid start"

#~ msgid "Find..."
#~ msgstr "Sök..."

#~ msgid "Highlight text on the current website"
#~ msgstr "Markera text på nuvarande webbplats"

#~ msgid "Start without UI"
#~ msgstr "Starta utan användargränssnitt"

#~ msgid "geo location"
#~ msgstr "geografisk plats"

#~ msgid "the microphone"
#~ msgstr "mikrofonen"

#~ msgid "the camera"
#~ msgstr "kameran"

#~ msgid "camera and microphone"
#~ msgstr "kameran och mikrofonen"

#~ msgid "Ok"
#~ msgstr "Ok"
